1、配置管理页面
首先进入rabbitMQ安装目录，后进入sbin目录。
接着，使用rabbitmq-plugins.bat list查看已安装的插件列表，如上图。
接下来使用 rabbitmq-plugins.bat enable rabbitmq_management 命令开启网页版控制台。
最后，重启RabbitMQ服务生效。
在浏览器输入 http:localhost(ip):15672进入控制台，大功告成。


2、添加账号信息


创建用户并设置角色：
可以创建管理员用户，负责整个MQ的运维，例如：
[plain] view plain copy
$sudo rabbitmqctl add_user  user_admin  passwd_admin  
赋予其administrator角色：
[plain] view plain copy
$sudo rabbitmqctl set_user_tags user_admin administrator  
